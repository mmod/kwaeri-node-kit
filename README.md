# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-node-kit [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

node-kit is the entry point to the @kwaeri suite of components, which collectively provide a platform that makes it easy to develop applications with Node.js.

[![pipeline status](https://gitlab.com/kwaeri/node-kit/node-kit/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/node-kit/commits/main)  [![coverage report](https://gitlab.com/kwaeri/node-kit/node-kit/badges/main/coverage.svg)](https://kwaeri.gitlab.io/node-kit/node-kit/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)


## Table of Contents

| **MMOD DOCUMENTATION WIKI (node-kit SERIES)** |
|:------------------------------------------------------------------------------|
|[Introduction](Getting-Started-with-@kwaeri/Node-kit/)|
|[The History of NK](Getting-Started-with-@kwaeri/The-History-of-NK)|
|[How to Build a Kwaeri Application](Getting-Started-with-@kwaeri/How-to-Build-a-node-kit-Application)

## In this Article

* [A Prelude to Getting Started](#a-prelude-to-getting-started)
* [Development Status](#development-status)
* [The Implementation](#the-implementation)
  * [The Kit](#the-kit)
  * [The CLI](#the-cli)
  * [The XRM](#the-xrm)
  * [How it works](#how-it-works)

## A Prelude to Getting Started

kwaeri/node-kit is not ready for production, but published for testing and development purposes. It is in a beta state that is intended for allowing a larger audience of users to try out anything that may already be available, but please be aware that there is likely many aspects of the platform which are not working and/or completely broken; This is intended to allow users to find and report such issues so that they may be fixed. Updated documentation and complete examples and tutorials for getting started will be provided as the platform rewrite nears completion.

As an aside, the README.md file used to be very large and quite informal; while certain major aspects of the README.md file's intent  are going to be preserved - we will strive to get documentation migrated out to the [MMOD Documentation Project's wiki](https://gitlab.com/mmod/documentation/wikis/Getting-Started-with-@kwaeri/node-kit/) where it can eventually be a data-store or model for hydrating a more permanent - and future - documentation site.

## Development Status

Recently kwaeri/node-kit has seen some major changes which will break any existing projects developed with it. If this is the case for you, you'll want to stick to version `0.2.2` or older - though please be advised that the `0.2.x` pre-release channel will no longer see any support. Version `0.3.x` and newer is the current focus, and provides some welcomed changes that inche kwaeri/node-kit closer to a release candidate.

The entire project has been over-hauled and some much needed clean-up has been done. Previously, the project was nested under the `mmod` group on GitLab - but has now been migrated to an Open Source project (via the GitLab Open Source Program) in the `node-kit` sub-group of the `kwaeri` group.

As only node-kit related repositories will exist within this new hierarchy, we'll be making use of the project board for the entire sub-group in managing the - now fully - componentized platform. Issues can be reported directed under the respective component-module - as they'll propogate under the group's service desk.

If you have taken an interest in node-kit, and would like to see a focus on something that you feel is more important and/or useful to you in the near future, please submit an [issue](https://gitlab.com/kwaeri/node-kit/node-kit/issues).

## The Implementation

node-kit is the entry point to the kwaeri suite of components, which collectively provide a platform for application development with Node.js.

node-kit couples an original MV(a)C framework with a customizable application architecture, and completes the pairing with extensible end-to-end tooling.

### The Kit

node-kit's MVC framework offers several application architectures.

* Use a kwaeri REST application for your back-end application, and leverage React - in a kwaeri React app - for your front-end.
* Use a Kwaeri MV(a)C application, which offers built-in support for themes (called 'layouts'), modules, and comes complete with a HTML Template Engine. Render a response entirely server-side - or render an initial container server-side that's followed up with React, similar technologies, or a custom client-side application.
* Use a kwaeri XRM application as your product solution. The XRM is a two-faced application that implements and specializes entirely through its extension system.
* Choose from a variety of options for session support, or develop your own; REDIS is the latest addition to the session store collection, which includes Memcached, MySQL, PostgreSQL, a file-based solution. As a bottom line, one can spin up an application on the fly that leverages an in-memory store.
* Enjoy file-based routing.
* Every application template [per app architecture] is complimented by a full set of docker builds, compose definitions and supporting resources, and pipeline templates.
* ...to be continued.

### The CLI

The [`kue`](https://www.gitlab.com/kwaeri/user-executable) binary provides a project CLI (command-line interface) for developers to leverage with kwaeri projects. With the CLI, developers can save time they would otherwise spend on redundant processes and tasks. A subset of functionality the CLI provides:

* Generate a project, either a kwaeri MVC, kwaeri REST or kwaeri React application. Kwaeri XRM extension project templates are planned to be included with a future release.
* Generate template resources (i.e. REST endpoints) on the fly.
* Generate template React components on the fly (either presentation or container, with or without Redux support).
* Seed a database from the command line (using a seed, or `*.sql` file).
* Generate database migrations.
* Execute database migrations. It's easy to script kue within your containerized applications (when authoring pipelines), to - for instance - apply migrations as part of a pipeline. Examples of this are included in the project templates.
* Create an administrative user (when leveraging kwaeri's provided context-based user management).

The CLI leverages a configuration (`kwaeri.config.json` in React projects, or `kwaeri.{default|development|production}.json` in MVC/REST/XRM projects) through out the life of a kwaeri project to assist with generating content on a per-file basis, and with performing administrative tasks (i.e. migrations).

The CLI takes its functionality from extensions (specified in `cli.json`) through its publisher/subscriber system. The idea is similar to that of the Kwaeri XRM in that the extension system itself is the essence of the entire product, with its specializations provided by the extensions - the core functionality included.

Both the CLI and the XRM come with default core extensions; In the case of the CLI the extensions support the Kwaeri framework and applications specific to it - though what's to stop a developer from authoring extensions that turn `kue` into an express tooling or tooling for their favorite framework or DBMS?

***NOTE***: *The entry point to the user-executable has not yet been published. It's being worked on in tandem with the node-kit entry and as the template applications are updated to work with the rewrite of the platform, the user executable - and its extensions - will be adjusted, published, and released.*

### The XRM

With the XRM, one can wholly replace the core extension(s). The gist of the XRM is that its a Cross-Relational Management system, providing the foundational backbone and extension ecosystem needed to cater specifically to product specialization, rather than content management. Think of it like a CMS for developers.

The XRM has been fully tested, is tried and true, but is a complex beast under the hood; In rewriting the entire platform the XRM needed an overhaul too - it was implemented over the previously linear - and monolothic - platform and is being updated for the platform rewrite. This offers an opportunity to not only clean it up, but to improve efficiency and offer more in terms of functionality and versatility.

Look forward to its release!

### How it works

kwaeri/node-kit is the entry point for kwaeri applications. In rewriting the platform that is exactly how it is implemented. A local (to the project) copy of node-kit will be installed when `npm install .` is run after generating a project.

To install the tooling, run `npm install -g @kwaeri/user-executable`. This will give you a global install of the CLI ***(COMING SOON!)***.

A kwaeri project's structure is important when considering how project structure, files, and other content are generated. This can often be manipulated through the use of a configuration (`app.environment.json`) which is expected either at the root (React projects) - or within the `conf` (REST/XRM projects) - directory of any kwaeri project.

For a deeper understanding of a kwaeri application, especially the inner workings of the MVC framework - browse the respective documentation within the [MMOD Documentation Project's wiki](https://gitlab.com/mmod/documentation/wikis/Getting-Started-with-@kwaeri/node-kit/) (***Please note that this may be outdated at this point. Stay tuned!***)

### Usage

As we complete kwaeri/node-kit and release the `kue` CLI, this section will be expanded.

As an example of using node-kit from end-to-end, we'll build a REST project. Let's start by generating a new project:

```bash
kue new project --type rest MyRestProject
```

Let's enter the root of our new project:

```bash
cd my-rest-project
```

And add a new endpoint:

```bash
kue add end-point --type rest MyNewEndpoint
```

Let's say several iterations have happened and we are ready to manage some migrations, let us generate one:

```bash
kue add migration --type mysql AddUserToAppMigration
```

And after defining the migration we'll go ahead and apply it:

```bash
kue run migrations
```

Oops! We messed up, let's undo it:

```bash
kue run migrations --step-back 1
```

Finally, let's test the app (go ahead, set some break points first):

```bash
npm run debug
```


**NOTE**

As mentioned earlier, the README used to be a giant beast that spelled out everything and in a wordy way. The plan is to continue development of the platform and as we near a release candidate prepare some clear and clean documentation.


## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:contact-project+kwaeri-node-kit-node-kit-6974520-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
